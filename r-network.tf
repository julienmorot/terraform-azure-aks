resource "azurerm_virtual_network" "AKS-VNET" {
  name                = "AKS-VNET"
  location            = azurerm_resource_group.RG-WE-AKS.location
  resource_group_name = azurerm_resource_group.RG-WE-AKS.name
  address_space       = ["10.224.0.0/12"]

  tags = {
    environment = "Production"
  }
}

resource "azurerm_subnet" "AKS-Subnet" {
  name                 = "AKS-Subnet"
  resource_group_name = azurerm_resource_group.RG-WE-AKS.name
  virtual_network_name = azurerm_virtual_network.AKS-VNET.name
  address_prefixes     = ["10.224.0.0/16"]
}

resource "azurerm_network_security_group" "aks-agentpool-nsg" {
  name                = "aks-agentpool-nsg"
  location            = azurerm_resource_group.RG-WE-AKS.location
  resource_group_name = azurerm_resource_group.RG-WE-AKS.name

  tags = {
    Environment = "Production"
  }
}

resource "azurerm_subnet_network_security_group_association" "aks-agentpool-nsg-assoc" {
  subnet_id                 = azurerm_subnet.AKS-Subnet.id
  network_security_group_id = azurerm_network_security_group.aks-agentpool-nsg.id
}

resource "azurerm_lb" "aks-lb" {
  name                = "aks-lb"
  location            = azurerm_resource_group.RG-WE-AKS.location
  resource_group_name = azurerm_resource_group.RG-WE-AKS.name
}

