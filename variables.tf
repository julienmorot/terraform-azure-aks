variable "location" {
  type        = string
  default     = "westeurope"
  description = "Location of Azure resources"
}

variable "loc" {
  type        = string
  default     = "WE"
  description = "Short location of Azure resources"
}

variable "clustername" {
  type        = string
  default     = "aks"
  description = "Name of the AKS cluster"
}

variable "clusterdns" {
  type        = string
  default     = "aks"
  description = "DNS prefix Name of the AKS cluster"
}
