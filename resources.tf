resource "azurerm_resource_group" "RG-WE-AKS" {
  name     = "RG-${var.loc}-AKS"
  location = var.location
}

resource "random_string" "random" {
  length  = 8
  special = false
}

resource "azurerm_kubernetes_cluster" "K8S" {
  name                = "aks1"
  location            = azurerm_resource_group.RG-WE-AKS.location
  resource_group_name = azurerm_resource_group.RG-WE-AKS.name
  dns_prefix          = var.clusterdns

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
    vnet_subnet_id        = azurerm_subnet.AKS-Subnet.id
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Production"
  }
}


